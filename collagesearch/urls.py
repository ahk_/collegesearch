from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^college/(name|faculty)/([a-zA-Z0-9]+)', 'main.views.collegesearch'),
    url(r'^admin/', include(admin.site.urls)),
)
