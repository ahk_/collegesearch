from django.http import JsonResponse

from .models import *
# Create your views here.


def collegesearch(request, catagory, searchstring):

    output = []
    if catagory == "name":
        for college in College.objects.all():
            if searchstring.lower() in college.name.lower():
                output.append(college.slug)
    elif catagory == "faculty":
        for faculty in Faculty.objects.all():
            if searchstring.lower() in faculty.name.lower():
                output.append(faculty.collage.slug)

    return JsonResponse({'colleges': output}, safe=False)
