from django.db import models

# Create your models here.


class Collage(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    no_of_students = models.IntegerField()
    year_estd = models.IntegerField()

    def __str__(self):
        return self.name


class Faculty(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    year_started = models.IntegerField()
    collage = models.ForeignKey(Collage, default=None)
