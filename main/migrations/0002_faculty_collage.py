# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='faculty',
            name='collage',
            field=models.ForeignKey(default=None, to='main.Collage'),
            preserve_default=True,
        ),
    ]
