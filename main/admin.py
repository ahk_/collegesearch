from django.contrib import admin

from .models import Faculty, Collage

# Register your models here.


class FacultyInline(admin.TabularInline):
    model = Faculty
    extra = 1


@admin.register(Collage)
class CollageAdmin(admin.ModelAdmin):
    inlines = [FacultyInline]
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'year_estd', 'no_of_students']

    class Meta:
        model = Collage
